package main

import (
	"html/template"
	"log"
	"os"
)

func main(){
	tpl, err := template.ParseFiles("One.gmao")
	if err != nil {
		log.Fatalln(err)
	}
	
	err = tpl.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln(err)
	}
	
	tpl, err = tpl.ParseFiles("Two.gmao", "Vespa.gmao")
	if err != nil {
		log.Fatalln(err)
	}
	
	err = tpl.ExecuteTemplate(os.Stdout,"Vespa.gmao",nil)
	if err != nil {
		log.Fatalln(err)
	}
	
	err = tpl.ExecuteTemplate(os.Stdout,"two.gmao",nil)
	if err != nil {
		log.Fatalln(err)
	}
	
	err = tpl.ExecuteTemplate(os.Stdout,"one.gmao",nil)
	if err != nil {
		log.Fatalln(err)
	}
	
	err = tpl.Execute(os.Stdout,nil)
	if err != nil {
		log.Fatalln(err)
	}
	
	
}
