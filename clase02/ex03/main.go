package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	// a la hora de ejecutarlo en la temrinal debo pasar mi nombre como parametro
	// go run main.go christopher velazquez
	name := os.Args[1]
	fmt.Println(os.Args[0])
	fmt.Println(os.Args[1])

	str := fmt.Sprintf(`
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
	<title>Hello World!</title>
	</head>
	<body>
	<h1>` + name + `</h1>
	</body>
	</html>
	`)
	nf, err := os.Create("index.html")
	if err != nil {
		log.Fatal("Error creating file", err)
	}
	defer nf.Close()

	io.Copy(nf, strings.NewReader(str))
}
