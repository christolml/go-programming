package main

import (
	"fmt"
)

type secrectAgent struct {
	person
	licenseToKill bool
}

type person struct {
	fname string
	lname string
}

/* func (p person) Greeting() string {
	return fmt.Sprintf("Que onda %s %s", p.fname, p.lname)
} */

func (p person) speak() {
	fmt.Println(p.fname, p.lname, `says, "Good morning guys"`)
}

func (sa secrectAgent) speak() {
	fmt.Println(sa.fname, sa.lname, `says, "Shaken, not stirred."`)
}

type human interface {
	speak()
}

func saySomething(h human) {
	h.speak()
}

func main() {

	/* 	m := map[string]int{
	   		"Chris":  21,
	   		"Trosca": 12,
	   		"Rana":   10,
	   	}
	   	fmt.Println(m) */

	p1 := person{
		"chris",
		"velaz",
	}
	// p1.speak()
	// fmt.Println(p1.Greeting())

	sa1 := secrectAgent{
		person{
			"James",
			"Bond",
		},
		true,
	}
	/* 	sa1.speak()
	   	fmt.Println(sa1.person.Greeting())
	   	sa1.person.speak() */

	saySomething(p1)
	saySomething(sa1)

}
